﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bookopenscript : MonoBehaviour
{
    public Animator bookanimator;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MainCamera")
        {
            bookanimator.SetBool("BookOpen", true);
        }
        else
        {
            bookanimator.SetBool("BookOpen", false);
        }
    }
}

