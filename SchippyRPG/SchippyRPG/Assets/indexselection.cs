﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class indexselection : MonoBehaviour
{

    public Animator bookanimator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        Ray ray = Camera.main.ViewportPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 2f);

        RaycastHit hitInfo;
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
            if (hit.transform.tag == "Book")
            {
                bookanimator.SetBool("BookOpen", true);
            }
            else
            {
                bookanimator.SetBool("BookOpen", false);
            }

    }
}

